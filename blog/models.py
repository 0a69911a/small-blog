from django.contrib.auth import get_user_model
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils import timezone

User = get_user_model()


# class Tag(models.Model):
#     name = models.CharField(max_length=50)


class Post(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='posts')
    headline = models.CharField(max_length=250)
    text = models.TextField()
    creation_datetime = models.DateTimeField(default=timezone.now)
    # tags = models.ManyToManyField(Tag, related_name='posts')

    @property
    def short_text(self):
        """
        :return string from self.text not bigger than 400 symbols and not more than 60 words
        """
        return ' '.join(self.text.split(' ')[:60])[:400]

    @property
    def count_of_comments(self):
        return self.comments.count()

    @property
    def first_level_comments(self):
        return self.comments.filter(answer_to=None)


class Comment(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    text = models.CharField(max_length=256)
    answer_to = models.ForeignKey('self', null=True, on_delete=models.SET_NULL, related_name='answers')
    creation_datetime = models.DateTimeField(default=timezone.now)
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='comments')

    def nested_comments(self, level=0, *args):
        """
        :return: list with serialized comment, list of children comments and depth level
        """
        return [self.serialize_json(), [answer.nested_comments(level + 1, *args) for answer in self.answers.all()], level, *args]

    def serialize_json(self):
        return {
            'id': self.id,
            'author': str(self.author),
            'author_id': self.author_id,
            'text': self.text,
            'creation_datetime': self.creation_datetime.strftime('%Y-%m-%d %H:%M:%S'),
        }
