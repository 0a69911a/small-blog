from django import forms
from django.contrib import auth
from django.contrib.auth.forms import UserCreationForm
from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse, reverse_lazy
from django.views.generic import TemplateView, ListView, DetailView, CreateView, DeleteView

from blog.forms import UserRegistrationForm, CommentaryCreateForm, PostCreateForm
from blog.models import Comment, Post, User


class Feed(ListView):
    template_name = 'feed.html'
    context_object_name = 'post_list'
    model = Post
    queryset = Post.objects.select_related('author')
    paginate_by = 10
    ordering = '-creation_datetime'
    page_kwarg = 'page_number'


class UserCreation(TemplateView):
    template_name = 'registration/registration.html'
    model = User

    def get(self, request, *args, **kwargs):
        form = UserRegistrationForm()
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data['username']
            password = form.cleaned_data['password1']
            user = auth.authenticate(username=username, password=password)
            auth.login(request=request, user=user)
            return redirect(reverse('feed'))
        else:
            return render(request, self.template_name, {'form': form})


class DetailPost(DetailView):
    model = Post
    template_name = 'post_detail.html'


class CommentsView(ListView):
    model = Comment
    paginate_by = 10

    def get_queryset(self):
        request = self.request
        queryset = Post.objects.get(id=request.GET['post_id']).first_level_comments.order_by('creation_datetime')
        return queryset

    def get(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        paginator, page, object_list, has_other_pages = self.paginate_queryset(queryset,
                                                                               self.get_paginate_by(queryset))
        data_to_return = {
            'page': {
                'number': page.number,
                'has_next': page.has_next(),
                'has_previous': page.has_previous(),
                'pages_count': paginator.num_pages
            },
            'comments': [comment.nested_comments() for comment in object_list]
        }
        return JsonResponse(data_to_return)


class CommentaryCreate(CreateView):
    model = Comment
    post_id = forms.IntegerField()
    comment_id = forms.IntegerField()
    form_class = CommentaryCreateForm

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            self.model.objects.create(**form.cleaned_data, author=request.user)
            return JsonResponse({'status': 'ok'}, status=200)
        return JsonResponse(form.errors, status=500)


class CommentaryDelete(DeleteView):
    model = Comment

    def post(self, request, *args, **kwargs):
        self.success_url = request.POST.get('success_url', '/')
        return super().post(request, *args, **kwargs)


class UserDetail(DetailView):
    model = User
    template_name = 'user_detail.html'

    def get_context_data(self, **kwargs):
        context = super(UserDetail, self).get_context_data(**kwargs)
        context['post_list'] = self.object.posts.all()
        return context


class PostCreate(CreateView):
    model = Post
    template_name = 'new_post.html'
    success_url = reverse_lazy('feed')
    form_class = PostCreateForm

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        self.object = None
        if form.is_valid():
            self.object = self.model.objects.create(**form.cleaned_data, author=request.user)
            return HttpResponseRedirect(self.success_url)
        return self.form_invalid(form)


class PostDelete(DeleteView):
    model = Post
    success_url = reverse_lazy('feed')
    template_name = 'post_confirm_delete.html'
