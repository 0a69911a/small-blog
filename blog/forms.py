from django.contrib.auth.forms import UserCreationForm
from django import forms
from django.core.exceptions import NON_FIELD_ERRORS
from django.views.generic import CreateView

from blog.models import User, Comment, Post


class UserRegistrationForm(UserCreationForm):
    email = forms.EmailField(required=True)

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2')

    def save(self, commit=True):
        user = super().save(commit=False)
        user.email = self.cleaned_data['email']

        if commit:
            user.save()

        return user


class CommentaryCreateForm(forms.ModelForm):
    post_id = forms.CharField()
    answer_to_id = forms.CharField(required=False)

    class Meta:
        model = Comment
        fields = ('text', )

    def clean_text(self):
        text = self.cleaned_data['text']
        if '<script>' in text:
            raise forms.ValidationError('You are not allowed to use <script> tage here!')
        return text


class PostCreateForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ('text', 'headline')

    def clean_text(self):
        text = self.cleaned_data['text']
        if '<script>' in text:
            raise forms.ValidationError('You are not allowed to use <script> tage here!')
        return text

    def clean_headline(self):
        headline = self.cleaned_data['headline']
        if '<script>' in headline:
            raise forms.ValidationError('You are not allowed to use <script> tage here!')
        return headline
