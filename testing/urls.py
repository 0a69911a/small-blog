"""testing URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include, re_path

from blog.views import Feed, UserCreation, DetailPost, CommentsView, CommentaryCreate, CommentaryDelete, UserDetail, \
    PostCreate, PostDelete

urlpatterns = [
    path('admin/', admin.site.urls),
    re_path(r'^(?:(?P<page_number>\d+)/)?$', Feed.as_view(), name='feed'),
    re_path(r'^post/(?P<pk>\d+)/$', DetailPost.as_view(), name='post-detail'),
    path('post/new', PostCreate.as_view(), name='new_post'),
    re_path(r'^post/(?P<pk>\d+)/delete$', PostDelete.as_view(), name='delete_post'),

    # registration, login
    path('user/', include('django.contrib.auth.urls')),

    path('user/<int:pk>', UserDetail.as_view(), name='user'),
    path('register/', UserCreation.as_view(), name='register'),
    path('comment/', CommentsView.as_view(), name='comment'),
    path('add_comment/', CommentaryCreate.as_view(), name='add_comment'),
    path('comment/<int:pk>/delete', CommentaryDelete.as_view(), name='delete_comment'),
]
