# Small blog

Small Django blog app

## Getting Started

```
git clone https://eternalProcess@bitbucket.org/eternalProcess/small-blog.git
cd small-blog
source bin/activate
python manage.py makemigrations
python manage.py migrate
python manage.py runserver
```

## Built With

* [Python 3.7,2](https://www.python.org/downloads/release/python-372/)
* [Django 2.1.7](https://docs.djangoproject.com/en/2.1/) - used framework
